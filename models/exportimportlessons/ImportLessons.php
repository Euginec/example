<?php 
Yii::import("application.models.exportimportlessons.utils.*");
Yii::import("application.models.exportimportlessons.trait.ReadWriteUtilsTrait");

class ImportLessons extends BaseExportImportLessons
{
	use ReadWriteUtilsTrait;
	
	private $arrayQuestionsId = array();
	
	public function execute()
	{
		if($this->getLanguage() == 'en') {
			$this->setError("You selected the English language. The files can not be imported.");
			return false;
		}
		$this->handleFiles();
	}
	
	private function handleFiles()
	{
		$files = glob(Yii::getPathOfAlias('webroot') . $this->directoryForExportInport . '*.*');
		
		if(count($files) == 0) {
			$this->setError("The folder is empty.");
			return false;
		}
		
		foreach($files as $file) {
			$filename = ltrim(basename($file), '-');

			if($filename == '__lessons_groups.html')
				// Import groups of lessons
				$this->writeGroups(new ReadDataFromFile());
			else 		
				//Import articles of lessons
				$this->writeLessons($file, new ReadDataFromFile());

			$this->setReport("===========================================================================");
		}
		
		
	}
	private function writeGroups(ReadInterface $reader)
	{
		$file = '__lessons_groups.html';
		
		$reader->setFileName($file)->init();
		
		$arrayFileData = $reader->read();
		
		if(is_null($arrayFileData)) {
			$this->setError("The file <strong>$file</strong> isn\'t readable.");
			return false;
		}

		$transaction = Yii::app()->db->beginTransaction();
		
		foreach($arrayFileData as $data) {

			if(!$this->saveGroup($data)) {
				$transaction->rollback();
				$reader->close();
				$this->setError("The file <strong>$file</strong> isn't recorded in the database.");
				return false;
			}
		}
			
		$transaction->commit();
		
		$reader->close();
		$this->setReport("The file <strong>$file</strong> is recorded in the database.");
	}
	
	private function parserData(array $arrayData)
	{
		$data = array();
		$text = implode('', $arrayData);
	
		foreach($this->listOfRequiredFieldsTableArticles as $value) {
			$start  = trim($this->getFormatTagStart($value));
			$end    = trim($this->getFormatTagEnd($value));
				
			if(preg_match("|{$start}(.+?){$end}|is", $text, $matches))
				if(isset($matches[1]))
					$data['article'][$value] = trim($matches[1]);
		}
	
	
		$data['questions'] = $this->getBlockData($text, $this->blockQuestion);

		$questionsVariants = array();
		$blockQuestionVariants = $this->getBlockData($text, $this->blockQuestionVariants);
		if(!is_null($blockQuestionVariants)) {
			foreach($blockQuestionVariants as $block) {
				$questionsVariants = array_merge($questionsVariants, explode(PHP_EOL, trim($block)));
			}
		}
		$data['questions_variants'] = $questionsVariants;
		
		$questionsVariantsAnswers = array();
		$blockQuestionVariantsAnswers = $this->getBlockData($text, $this->blockQuestionVariantsAnswers);
		if(!is_null($blockQuestionVariantsAnswers)) {
			foreach($blockQuestionVariantsAnswers as $block) {
				$questionsVariantsAnswers = array_merge($questionsVariantsAnswers, explode(PHP_EOL, trim($block)));
			}
		}
		$data['questions_variants_answers'] = $questionsVariantsAnswers;

		return $data;
	
	}
	
	private function writeLessons($file, ReadInterface $reader)
	{
		$reader->setFilePath($file)->init();
		
		$file = ltrim(basename($file), '-');
		
		$arrayFileData = $reader->read();

		if(!is_array($arrayFileData)) {
			$this->setReport("The file <strong style=\"color:red;\">$file</strong> is empty.");
			return true;
		}
		
		$arrDataForSave = $this->parserData($arrayFileData);
		
		$transaction = Yii::app()->db->beginTransaction();
		
		try {
			$idArticle = $this->saveLessons($arrDataForSave['article'], $file);

			if($idArticle === false) {
				throw new Exception("Data didn't save. (Article)");
			}
			
			foreach($arrDataForSave['questions'] as $data) {
				$idQuestion = $this->saveQuestions($data, $idArticle);

				if($idQuestion === false) {
					throw new Exception("Data didn't save. (Quiestion)");
				}
				
				if(!$this->saveQuestionsVariantsAnswers($arrDataForSave['questions_variants_answers'], $idQuestion))
					throw new Exception("Data didn't save. (Variant Answer)");

				if(!$this->saveQuestionsVariants($arrDataForSave['questions_variants'], $idQuestion))
					throw new Exception("Data didn't save. (Variants)");
				
			}
			
			$transaction->commit();
			$this->setReport("The file <strong>$file</strong> is recorded in the database.");
		}
		catch(Exception $e) {
			$transaction->rollback();
			$this->setReport($e->getMessage());
			$this->setReport("The file <strong>$file</strong> isn't recorded in the database.");
		}
		
		$reader->close();
		
		
		
	}
	
	private function saveLessons($data, $file = '')
	{
		if(count($data) == 0 || empty($file))
			return false;
		
		$url = str_replace('.html', '', trim($file));

		$enArticle = LessonsArticlesLanguage::model()->find(array(
				"condition" => "url = :url AND language = :language",
				"params" => array (
						":url" => $url,
						":language" => 'en'
				)
		));
		
		if(is_null($enArticle)) {
			$this->setReport("The English record this file <strong style=\"color:red;\">$file</strong> don't exist in the article table.");
			return false;
		}
		
		//var_dump($enArticle);

		$oldArticle = LessonsArticlesLanguage::model()->find(array(
				"condition" => "parent_id = :parent_id AND language = :language",
				"params" => array (
						":parent_id" => $enArticle->parent_id,
						":language" => $this->getLanguage()
				)
		));
		
		if(!is_null($oldArticle)) {
			$this->setReport("The record this file <strong style=\"color:red;\">$file</strong> exist in the article table.");
			return false;
		}
		
		$groupId = LessonsGroupsLanguage::model()->find(array(
				"select" => "id",
				"condition" => "parent_id in (SELECT parent_id FROM lessons_groups_language WHERE id = :id AND language = 'en') AND language = :language",
				"params" => array (
						":id" => $enArticle->group_id,
						":language" => $this->getLanguage() 
				)
		));

		if(is_null($groupId))
			return false; 
		
		$article = new LessonsArticlesLanguage();
		$article->parent_id = $enArticle->parent_id;
		$article->language = $this->getLanguage();
		$article->group_id = $groupId->id;
		$article->name = trim($data['name']);
		$article->url = $this->makeUrl($data['name']);
		$article->body = $data['body'];
		$article->title = $data['title'];
		$article->keywords = $enArticle->keywords;
		$article->meta_description = $enArticle->meta_description;
		$article->active = 0;
		$article->order = $enArticle->order;
		$article->good_result = $data['good_result'];
		$article->very_good_result = $data['very_good_result'];
		
		if(!$article->save())
			return false;
		else 
			return $article->id;
	}
	
	private function saveQuestions($data, $id)
	{
		if(empty($data) || $id === false)
			return false;
		
		$arrData = explode($this->separateForString, $data);
		
		if(count($arrData) < 2)
			return false;

		$enQuestion = LessonsQuestionsArticles::model()->find(array(
				"condition" => "id = :id",
				"params" => array (
						":id" => trim($arrData[0]),
				)
		));


		if(is_null($enQuestion))
			return false;

		
		$question = new LessonsQuestionsArticles();
		$question->article_id = $id;
		$question->name = trim($arrData[1]);
		$question->answer = $enQuestion->answer;
		$question->points = $enQuestion->points;
		$question->type_answer = $enQuestion->type_answer;
		$question->order = $enQuestion->order;
		$question->active = 1;

		if(!$question->save())
			return false;
		else {
			$this->arrayQuestionsId[$question->id] = $arrData[0];
				
			return $question->id;
		}
		
	}
	
	private function saveQuestionsVariantsAnswers($dataArray, $id)
	{
		if(is_null($dataArray) || $id === false)
			return true;
		
		foreach($dataArray as $data) {
			$arrData = explode($this->separateForString, $data);
				
			if(count($arrData) < 2)
				return false;

			$enVariants = LessonsQuestionsVariantsAnswers::model()->find(array(
					"condition" => "id = :id",
					"params" => array (
							":id" => trim($arrData[0]),
					)
			));

			if(is_null($enVariants))
				return false;
			
			if(isset($this->arrayQuestionsId[$id]) && $this->arrayQuestionsId[$id] == $enVariants->question_id) {
				$variant = new LessonsQuestionsVariantsAnswers();
				$variant->question_id = $id;
				$variant->name = trim($arrData[1]);
				$variant->index = $enVariants->index;
				$variant->active = 1;
								
				if(!$variant->save())
					return false;
			}
			else {
				continue;
			}
		}

		return true;
	}
	
	
	private function saveQuestionsVariants($dataArray, $id)
	{
		if(is_null($dataArray) || $id === false)
			return true;
		
		foreach($dataArray as $data) {
			$arrData = explode($this->separateForString, $data);
				
			if(count($arrData) < 2)
				return false;

				$enVariants = LessonsQuestionsVariants::model()->find(array(
						"condition" => "id = :id",
						"params" => array (
								":id" => trim($arrData[0]),
						)
				));


			if(is_null($enVariants))
				return false;
					
			if(isset($this->arrayQuestionsId[$id]) && $this->arrayQuestionsId[$id] == $enVariants->question_id) {
				$variant = new LessonsQuestionsVariants();
				$variant->question_id = $id;
				$variant->name = trim($arrData[1]);
				$variant->index = $enVariants->index;
				$variant->active = 1;
					
				if(!$variant->save())
					return false;
			}
			else {
				continue;
			}
		}
	
		return true;
	}
	
	
	private function saveGroup($data)
	{
		if(is_string($data)) {
			
			$arrData = explode($this->separateForString, $data);
			
			if(count($arrData) < 2)
				return false;

			$oldGroup = LessonsGroupsLanguage::model()->find(array(
					"condition" => "name = :name AND language = :language",
					"params" => array (
							":name" => trim($arrData[1]),
							":language" => $this->getLanguage()
					)
			));
			
			if(!is_null($oldGroup)) {
				$this->setReport("The record with this name <strong style=\"color:red;\">$arrData[1]</strong> exist in the groups table.");
				return true;
			}

			$enGroup = LessonsGroupsLanguage::model()->find(array(
					"condition" => "id = :id AND language = :language",
					"params" => array (
							":id" => $arrData[0],
							":language" => 'en'
					)
			));
			
			if(is_null($enGroup))
				return false;

			$group = new LessonsGroupsLanguage();
			$group->parent_id = $enGroup->parent_id;
			$group->language = $this->getLanguage();
			$group->name = trim($arrData[1]);
			$group->active = 0;
			$group->order = $enGroup->order;
			$result = $group->save();
			
			if($result)
				$this->setReport("The record with this name <strong style=\"color:green;\">$arrData[1]</strong> is added to the groups table.");
			
			return $result;
		}
	}
	
	private function getBlockData($text, $tagName)
	{
		if(empty($tagName))
			return null;
		
		$start  = trim($this->getFormatTagStart($tagName));
		$end    = trim($this->getFormatTagEnd($tagName));
		
		if(preg_match_all("|{$start}(.+?){$end}|is", $text, $matches))
			if(isset($matches[1]))
				return $matches[1];
			else 
				return null;
		
	}
	
	private function makeUrl($text)
	{
		$text = preg_replace('|[^\p{L}\d\s]*|u','', mb_strtolower($text));
		return preg_replace('| +|', '-', $text);
		
	}
}
?>
