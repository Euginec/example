<?php
Yii::import("application.models.exportimportlessons.utils.*");

class ExportLessons extends BaseExportImportLessons
{
	public function execute()
	{
		$this->deleteFiles();
		
		// Retrieve groups of lessons
		$groups = $this->getGroups();
		// Export groups of lessons
		$this->writeGroups($groups, new WriteDataToFile());

		// Retrieve articles of lessons
		$articles = $this->getArticles();
		//Export articles of lessons
		$this->writeLessons($articles, new WriteDataToFile());

	}

	private function writeGroups($groups, WriteInterface $writer)
	{
		$file = '__lessons_groups.html';
		
		$writer->setFileName($file)->init();

		if(!is_null($groups)) {
			foreach($groups as $group) {
				if(!$writer->write($group->id. $this->separateForString . $group->name . PHP_EOL)) {
					$writer->close();
					$this->setError("The file <strong>$file</strong> isn't saved.");
					return false;
				}
			}
		}

		$writer->close();
		$this->setReport("The file <strong>$file</strong> saved.");
	}

	private function writeLessons($articles, WriteInterface $writer)
	{
		foreach($articles as $article) {
			$arrayValues = array();
				
			$file = $article->url . '.html';
			
			// Set file name and open a file
			$writer->setFileName($file)->init();
				
			foreach($article as $key => $value) {
				if(in_array($key, $this->listOfRequiredFieldsTableArticles)) {
					$arrayValues[$key] = $value;
				}
			}
				
			if(count($arrayValues))
				if(!$writer->write($arrayValues)) {
					$writer->close();
					$this->setError("The file <strong>$file</strong> isn't saved.");
					continue;
				}
			
			if(!$this->writeQuestions($article->id, $writer)) {
				$writer->close();
				$this->setError("The file <strong>$file</strong> isn't saved.");
				continue;
			}
			
			// close file
			$writer->close();
			$this->setReport("The file <strong>$file</strong> saved.");

		}
	}
	
	private function writeQuestions($id, WriteInterface $writer)
	{
		$qiestions = $this->getQuestions($id);
			
		if(!is_null($qiestions)) {
		
			foreach($qiestions as $key => $question) {
				$writer->write("  " . PHP_EOL);
				$writer->write("  " . PHP_EOL);
				$writer->write("  " . PHP_EOL);
				$writer->write("  " . PHP_EOL);
				$writer->write("  " . PHP_EOL);
				
				if(!$writer->write($writer->getFormatTagStart($this->blockQuestion)) ||
					!$writer->write($question->id. $this->separateForString . $question->name . PHP_EOL) ||
					!$writer->write($writer->getFormatTagEnd($this->blockQuestion))) {
						return false;
				}
				
				if(!$this->writeQuestionsVariants($question->id, $writer) ||
					!$this->writeQuestionsVariantsAnswers($question->id, $writer)) {
						return false;
						
				}
		
			}
		}
		
		return true;
	}

	private function writeQuestionsVariants($id, WriteInterface $writer)
	{
		$questionsVariants = $this->getQuestionsVariants($id);
	

		if(count($questionsVariants) > 0) {
			// Open a block of questions variants
			$writer->write($writer->getFormatTagStart($this->blockQuestionVariants));
	
			foreach($questionsVariants as $key => $questionVariant) {
				if(!$writer->write($questionVariant->id. $this->separateForString . $questionVariant->name . PHP_EOL))
					return false;
			}
	
			// End a block of questions variants
			$writer->write($writer->getFormatTagEnd($this->blockQuestionVariants));
		}
		
		return true;
	}

	private function writeQuestionsVariantsAnswers($id, WriteInterface $writer)
	{
		$questionsVariants = $this->getQuestionsVariantsAnswers($id);
	

		if(count($questionsVariants) > 0) {
			// Open a block of questions variants
			$writer->write($writer->getFormatTagStart($this->blockQuestionVariantsAnswers));
	
			foreach($questionsVariants as $key => $questionVariant) {
				if(!$writer->write($questionVariant->id. $this->separateForString . $questionVariant->name . PHP_EOL))
					return false;
			}
	
			// End a block of questions variants
			$writer->write($writer->getFormatTagEnd($this->blockQuestionVariantsAnswers));
		}
		
		return true;
	}
	
	private function getGroups()
	{
		return LessonsGroupsLanguage::model()->findAll(array(
				"condition" => "language = :language",
				"params" => array (
						":language" => $this->getLanguage()
				)
		));
	}

	private function getArticles()
	{
		return LessonsArticlesLanguage::model()->findAll(array(
				"condition" => "language = :language",
				"params" => array (
						":language" => $this->getLanguage()
				)
		));
	}

	private function getQuestions($id)
	{
		return LessonsQuestionsArticles::model()->findAll(array(
				"condition" => "article_id = :article_id",
				"params" => array (
						":article_id" => $id
				)
		));
	}

	private function getQuestionsVariants($id)
	{
		return LessonsQuestionsVariants::model()->findAll(array(
				"condition" => "question_id = :question_id",
				"params" => array (
						":question_id" => $id
				)
		));
	}

	private function getQuestionsVariantsAnswers($id)
	{
		return LessonsQuestionsVariantsAnswers::model()->findAll(array(
				"condition" => "question_id = :question_id",
				"params" => array (
						":question_id" => $id
				)
		));
	}
}
?>
