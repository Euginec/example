<?php 

trait ConfigTrait
{
	public $separateForString = ':::';
	
	public $directoryForExportInport = '/path/to/lessons/';
	
	public $listOfRequiredFieldsTableArticles = array('title', 'name', 'body', 'good_result', 'very_good_result');
	
	public $listOfRequiredFieldsTableQiestionsArticles = array('id', 'name');
	
	//public $blockQuestions = "BLOCK_QUESTIONS";

	public $blockQuestion = "BLOCK_QUESTION";
	
	public $blockQuestionVariants = "BLOCK_QUESTION_VARIANTS";

	public $blockQuestionVariantsAnswers = "BLOCK_QUESTION_VARIANTS_ANSWERS";
}
?>
