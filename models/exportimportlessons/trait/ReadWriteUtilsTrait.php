<?php 

trait ReadWriteUtilsTrait
{
	private $file = null;
	private $filename = null;
	private $filepath = null;
	
	protected function getFile() {
		return $this->file;
	}
	
	protected function setFile($file) {
		$this->file = $file;
		return $this;
	}
	
	public function getFilePath() {
		return $this->filepath;
	}
	
	public function setFilePath($filepath) {
		$this->filepath = $filepath;
		return $this;
	}
	
	public function getFileName() {
		return $this->filename;
	}
	
	public function setFileName($filename) {
		$this->filename = $filename;
		return $this;
	}
	
	public function getFormatTagStart($text)
	{
		return $this->getFormatTag(' START_' . $text);
	}
	
	public function getFormatTagEnd($text)
	{
		return $this->getFormatTag(' END_' . $text) . PHP_EOL;
	}
	
	protected function getFormatTag($tag)
	{
		return strtoupper($this->separateForString . $tag . ' ' . $this->separateForString) . PHP_EOL;
	}
	
}
?>
