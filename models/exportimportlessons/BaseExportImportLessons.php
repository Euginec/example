<?php 
Yii::import("application.models.exportimportlessons.trait.ConfigTrait");

class BaseExportImportLessons
{
	use ConfigTrait;
	
	private $messages = array();
	private $reports = array();
	
	public function init()
	{
		if(!Yii::app()->request->isAjaxRequest){
			echo '';
			Yii::app()->end();
		}
	
		if(empty($this->getLanguage()))
			$this->setError('Language isn\'t specified.');
	
			$errors = $this->getErrors();
			if(!is_null($errors)) {
				echo $errors;
				Yii::app()->end();
			}
	
	}
	public function getLanguage()
	{
		return strtolower(Yii::app()->request->getPost('lang'));
	}
	
	public function setError($data)
	{
		if(is_string($data))
			$this->messages[] = $data;
	
	
		if(is_array($data))
			foreach($data as $d)
				$this->messages[] = $d;
	}
	
	public function getErrors()
	{
		if(count($this->messages) == 0)
			return null;
		else
			return $this->messages;
	}

	public function setReport($data)
	{
		if(is_string($data))
			$this->reports[] = $data;
	
	
		if(is_array($data))
			foreach($data as $d)
				$this->reports[] = $d;
	}
	
	public function getReports()
	{
		if(count($this->reports) == 0)
			return null;
		else
			return $this->reports;
	}
	
	public function getAjaxReports()
	{
		return json_encode(array(
			'status' => (count($this->messages) == 0) ? 'success' : 'error',
			'errors' => $this->messages,
			'reports' => $this->reports,
		));
	}
	
	protected function deleteFiles()
	{
		$files = glob(Yii::getPathOfAlias('webroot') . $this->directoryForExportInport . '*.*');
	
		foreach($files as $file)
			if(file_exists($file))
				unlink($file);
	}
}
?>