<?php 
Yii::import("application.models.exportimportlessons.trait.ConfigTrait");
Yii::import("application.models.exportimportlessons.trait.ReadWriteUtilsTrait");

abstract class ReadInterface
{
	use ConfigTrait, ReadWriteUtilsTrait;
	
	abstract public function init();
	
	abstract public function read();
	
	abstract public function close();
}
?>