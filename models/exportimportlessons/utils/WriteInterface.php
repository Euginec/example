<?php 
Yii::import("application.models.exportimportlessons.trait.ConfigTrait");
Yii::import("application.models.exportimportlessons.trait.ReadWriteUtilsTrait");

abstract class WriteInterface
{
	use ConfigTrait, ReadWriteUtilsTrait;
	
	abstract public function init();
	
	abstract public function write($data);
	
	abstract public function close();
}
?>