<?php 

class ReadDataFromFile extends ReadInterface
{
	public function init()
	{
		if($this->getFileName() != '')
			$this->setFilePath(Yii::getPathOfAlias('webroot') . $this->directoryForExportInport . $this->getFileName());

		$this->setFile(fopen($this->getFilePath(), "r"));
	}
	
	public function read()
	{
		$buffer = null;
		
		$f = $this->getFile();

		if(!$f)
			return null;
		
		while (!feof($f)) {
			$tmp = fgets($f);
			if($tmp)
				$buffer[] = $tmp;
			
		}

		return $buffer;
	}
	
	public function close()
	{
		fclose($this->getFile());
		$this->setFile(null);
	}
}
?>