<?php 

class WriteDataToFile extends WriteInterface
{
	public function init()
	{
		$this->setFilePath(Yii::getPathOfAlias('webroot') . $this->directoryForExportInport . $this->getFileName());

		$this->setFile(fopen($this->getFilePath(), "w+"));
	}
	
	public function write($data)
	{
		$f = $this->getFile();
		
		if(!$f)
			return false;
		
		if(is_string($data))
			if(!fwrite($f, $data))
				return false;
			else
				return true;
		
		if(is_array($data)) {
			foreach($data as $key => $value) {
				if(!fwrite($f, $this->getFormatTagStart($key)) ||
					!fwrite($f, $value . PHP_EOL) ||
					!fwrite($f, $this->getFormatTagEnd($key)))
						return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	public function close()
	{
		fclose($this->getFile());
		$this->setFile(null);
	}
}
?>