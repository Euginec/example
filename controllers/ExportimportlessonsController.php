<?php
Yii::import("application.models.exportimportlessons.*");

class ExportimportlessonsController extends BaseAdminController {
    public $namePage;
    
    /*
    *
    */
	public function actionIndex()
	{
        $model = new LessonsArticlesLanguage('searchArticlesLessons');
		$this->render('index', array('model' => $model));
	}
	
	public function actionExport()
	{
		$export = new ExportLessons();
		$export->init();
		
		$export->execute();
		
		echo $export->getAjaxReports();
		
		Yii::app()->end();
	}

	public function actionImport()
	{
		$import = new ImportLessons();
		$this->init();
		
		$import->execute();
		
		echo $import->getAjaxReports();
		
		Yii::app()->end();
	}
}
